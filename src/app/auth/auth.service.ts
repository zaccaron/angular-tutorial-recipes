import { Router } from "@angular/router";
import { Injectable } from "@angular/core";

@Injectable()
export class AuthService {
    token: string;

    constructor(private router: Router) { }

    signUpUser(email: string, password: string) {
        //criar request para criar usuario
    }

    signInUser(email: string, password: string) {
        //criar request para autenticar usuário
        this.router.navigate(['/']);
    }

    getToken() {

    }

    isAuthenticated() {

        //return this.token != null;
        return true
    }

    logout() {
        this.token = null;
    }
}